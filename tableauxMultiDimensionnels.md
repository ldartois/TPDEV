<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>
DEV 1.1
    </title>
  </head>
  <body>

<h3>
Tableaux multidimensionnels
</h3>

<div class="accordion" id="rappels">
  <div class="accordion-group">
    <div class="accordion-heading">
      <a class="accordion-toggle" href="#rappel1"
         data-parent="#rappels" data-toggle="collapse">
        <span class="label label-info">
          <i class="icon-search icon-white">
          </i>
&Agrave; savoir&nbsp;
        </span>
Définir plusieurs dimensions pour un tableau
      </a>
    </div>
    <div id="rappel1" class="accordion-body collapse">
      <div class="accordion-inner">
        <p>
Jusqu'ici, un tableau nous permet de stocker de nombreuses données, et d'identifier
chacune de ces données à l'aide d'un indice numérique. Un tel tableau n'est donc qu'une
simple liste.
        </p>
        <p>
Mais le terme de tableau évoque en général plutôt une grille (comme dans un
<em>tableur</em>). Dans une telle organisation, on identifie une donnée par sa ligne et sa
colonne, c'est à dire à l'aide de deux indices.
        </p>
        <p>
On peut simuler une grille par un <em>tableau de tableaux</em>, mais c'est compliqué à
mettre en place. Le plus simple est de déclarer un <em>tableau à deux dimensions</em>.
        </p>
        <p>
<pre class="c source-c"><span class="kw4">int</span> grille<span class="br0">&#91;</span><span class="nu0">4</span><span class="br0">&#93;</span><span class="br0">&#91;</span><span class="nu0">5</span><span class="br0">&#93;</span><span class="sy0">;</span> <span class="coMULTI">/* tableau de 20 cases */</span>
&nbsp;
grille<span class="br0">&#91;</span><span class="nu0">1</span><span class="br0">&#93;</span><span class="br0">&#91;</span><span class="nu0">2</span><span class="br0">&#93;</span> <span class="sy0">=</span> <span class="nu0">54</span><span class="sy0">;</span></pre>        </p>
        <p>
Dans cet exemple, nous aurions pu déclarer un tableau de capacité 20 et chaque case
aurait été identifiée par un indice entre 0 et 19.  Mais dans le tableau <tt>grille</tt>,
qui possède deux capacités (4 et 5), chaque case est identifiée par deux indices : le
premier entre 0 et 3, le second entre 0 et 4. Le sens que vous choisissez de donner à ces
indices dépend du problème : ligne et colonne, jour et semaine, album et plage...
        </p>
        <p>
<pre class="c source-c"><span class="kw4">int</span> grille<span class="br0">&#91;</span><span class="nu0">4</span><span class="br0">&#93;</span><span class="br0">&#91;</span><span class="nu0">5</span><span class="br0">&#93;</span> <span class="sy0">=</span> <span class="br0">&#123;</span><span class="br0">&#123;</span><span class="nu0">70</span><span class="sy0">,</span> <span class="nu0">72</span><span class="sy0">,</span> <span class="nu0">74</span><span class="sy0">,</span> <span class="nu0">76</span><span class="sy0">,</span> <span class="nu0">78</span><span class="br0">&#125;</span><span class="sy0">,</span>
                    <span class="br0">&#123;</span><span class="nu0">50</span><span class="sy0">,</span> <span class="nu0">52</span><span class="sy0">,</span> <span class="nu0">54</span><span class="sy0">,</span> <span class="nu0">56</span><span class="sy0">,</span> <span class="nu0">58</span><span class="br0">&#125;</span><span class="sy0">,</span>
                    <span class="br0">&#123;</span><span class="nu0">30</span><span class="sy0">,</span> <span class="nu0">32</span><span class="sy0">,</span> <span class="nu0">34</span><span class="sy0">,</span> <span class="nu0">36</span><span class="sy0">,</span> <span class="nu0">38</span><span class="br0">&#125;</span><span class="sy0">,</span>
                    <span class="br0">&#123;</span><span class="nu0">10</span><span class="sy0">,</span> <span class="nu0">12</span><span class="sy0">,</span> <span class="nu0">14</span><span class="sy0">,</span> <span class="nu0">16</span><span class="sy0">,</span> <span class="nu0">18</span><span class="br0">&#125;</span><span class="br0">&#125;</span><span class="sy0">;</span></pre>        </p>
        <p>
On peut initialiser un tableau à deux dimensions par une liste de listes
d'initialisation. Des retours à la ligne judicieux permettent alors de &laquo;voir&raquo;
les deux dimensions. Cependant, la mémoire est linéaire donc les cases sont en réalité
juste à la suite les unes des autres :
        </p>
        <p>
          <table class="table table-bordered">
            <colgroup span="20" style="width:5%">
            </colgroup>
            <tbody>
              <tr>
                <td>
<div class="text-center">70</div>
                </td>
                <td>
<div class="text-center">72</div>
                </td>
                <td>
<div class="text-center">74</div>
                </td>
                <td>
<div class="text-center">76</div>
                </td>
                <td>
<div class="text-center">78</div>
                </td>
                <td>
<div class="text-center">50</div>
                </td>
                <td>
<div class="text-center">52</div>
                </td>
                <td>
<div class="text-center">54</div>
                </td>
                <td>
<div class="text-center">56</div>
                </td>
                <td>
<div class="text-center">58</div>
                </td>
                <td>
<div class="text-center">30</div>
                </td>
                <td>
<div class="text-center">32</div>
                </td>
                <td>
<div class="text-center">34</div>
                </td>
                <td>
<div class="text-center">36</div>
                </td>
                <td>
<div class="text-center">38</div>
                </td>
                <td>
<div class="text-center">10</div>
                </td>
                <td>
<div class="text-center">12</div>
                </td>
                <td>
<div class="text-center">14</div>
                </td>
                <td>
<div class="text-center">16</div>
                </td>
                <td>
<div class="text-center">18</div>
                </td>
              </tr>
            </tbody>
          </table>
        </p>
        <p>
Ce mécanisme peut être étendu pour créer des tableaux à trois ou quatre dimensions (ou
même plus), mais c'est peu fréquent.
        </p>
      </div>
    </div>
  </div>
</div>

<ol>
  <li>
    <p>
<strong>Progressions.</strong> Écrivez un programme où les tableaux <tt>t1</tt>, <tt>t2</tt>
et <tt>t3</tt> contiennent les valeurs suivantes :
    </p>
    <p>
      <div class="row">
        <div class="span2">
          <table class="table table-bordered">
            <caption>
<tt>t1</tt>
            </caption>
            <colgroup span="5" style="width:20%">
            </colgroup>
            <tbody>
              <tr>
                <td>
<div class="text-center">1</div>
                </td>
                <td>
<div class="text-center">2</div>
                </td>
                <td>
<div class="text-center">3</div>
                </td>
                <td>
<div class="text-center">4</div>
                </td>
                <td>
<div class="text-center">5</div>
                </td>
              </tr>
              <tr>
                <td>
<div class="text-center">1</div>
                </td>
                <td>
<div class="text-center">2</div>
                </td>
                <td>
<div class="text-center">3</div>
                </td>
                <td>
<div class="text-center">4</div>
                </td>
                <td>
<div class="text-center">5</div>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
        <div class="span2 offset1">
          <table class="table table-bordered">
            <caption>
<tt>t2</tt>
            </caption>
            <colgroup span="5" style="width:20%">
            </colgroup>
            <tbody>
              <tr>
                <td>
<div class="text-center">1</div>
                </td>
                <td>
<div class="text-center">2</div>
                </td>
                <td>
<div class="text-center">3</div>
                </td>
                <td>
<div class="text-center">4</div>
                </td>
                <td>
<div class="text-center">5</div>
                </td>
              </tr>
              <tr>
                <td>
<div class="text-center">6</div>
                </td>
                <td>
<div class="text-center">7</div>
                </td>
                <td>
<div class="text-center">8</div>
                </td>
                <td>
<div class="text-center">9</div>
                </td>
                <td>
<div class="text-center">10</div>
                </td>
              </tr>
              <tr>
                <td>
<div class="text-center">11</div>
                </td>
                <td>
<div class="text-center">12</div>
                </td>
                <td>
<div class="text-center">13</div>
                </td>
                <td>
<div class="text-center">14</div>
                </td>
                <td>
<div class="text-center">15</div>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
        <div class="span2 offset1">
          <table class="table table-bordered">
            <caption>
<tt>t3</tt>
            </caption>
            <colgroup span="5" style="width:20%">
            </colgroup>
            <tbody>
              <tr>
                <td>
<div class="text-center">0</div>
                </td>
                <td>
<div class="text-center">0</div>
                </td>
                <td>
<div class="text-center">0</div>
                </td>
                <td>
<div class="text-center">0</div>
                </td>
                <td>
<div class="text-center">0</div>
                </td>
              </tr>
              <tr>
                <td>
<div class="text-center">1</div>
                </td>
                <td>
<div class="text-center">0</div>
                </td>
                <td>
<div class="text-center">0</div>
                </td>
                <td>
<div class="text-center">0</div>
                </td>
                <td>
<div class="text-center">0</div>
                </td>
              </tr>
              <tr>
                <td>
<div class="text-center">1</div>
                </td>
                <td>
<div class="text-center">2</div>
                </td>
                <td>
<div class="text-center">0</div>
                </td>
                <td>
<div class="text-center">0</div>
                </td>
                <td>
<div class="text-center">0</div>
                </td>
              </tr>
              <tr>
                <td>
<div class="text-center">1</div>
                </td>
                <td>
<div class="text-center">2</div>
                </td>
                <td>
<div class="text-center">3</div>
                </td>
                <td>
<div class="text-center">0</div>
                </td>
                <td>
<div class="text-center">0</div>
                </td>
              </tr>
              <tr>
                <td>
<div class="text-center">1</div>
                </td>
                <td>
<div class="text-center">2</div>
                </td>
                <td>
<div class="text-center">3</div>
                </td>
                <td>
<div class="text-center">4</div>
                </td>
                <td>
<div class="text-center">0</div>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </p>
    <p>
Utilisez des boucles (pas des listes) pour placer ces valeurs dans les tableaux.
Affichez les tableaux les uns au-dessus des autres.
Puis affichez-les à nouveau, en inversant lignes et colonnes.
    </p>
  </li>
  <li>
    <p>
<strong>Horizontal.</strong> Reprenez l'exercice précédent, et cette fois affichez les
tableaux côte-à-côte (comme sur la figure ci-dessus).
    </p>
  </li>
  <li>
    <p>
<strong>Triangle.</strong> Écrivez un programme qui place dans un tableau à deux
dimensions un triangle de Pascal de hauteur 30 puis l'affiche.
    </p>
    <p>
<pre>
 1
 1   1
 1   2   1
 1   3   3   1
 1   4   6   4   1
 1   5  10  10   5   1
 1   6  15  20  15   6   1
 ...
</pre>
    </p>
    <p>
On rappelle la relation de récurrence :
      <center>
<img src="binomial.svg" alt="" />
      </center>
    </p>
  </li>
  <li>
    <p>
<i class="icon-time"></i> <strong>Balayage.</strong> Reprenez l'exercice précédent, en
utilisant cette fois un tableau à une seule dimension.
    </p>
  </li>
  <li>
    <p>
<i class="icon-time"></i> <strong>Magique.</strong> Un carré magique est une grille de
trois lignes et trois colonnes contenant tous les chiffres de 1 à 9. Si on additionne les
trois chiffres sur une ligne, une colonne ou une diagonale, on obtient toujours la même
valeur.
    </p>
    <p>
<pre>
8  3  4
1  5  9
6  7  2
</pre>
    </p>
    <p>
Écrivez un programme qui demande à l'utilisateur la valeur à mettre dans chacune des neuf
cases, puis indique si le carré est magique. On s'attachera à vérifier que :
      <ul>
        <li>
chaque valeur est comprise entre 1 et 9,
        </li>
        <li>
aucune valeur n'est répétée,
        </li>
        <li>
les sommes des lignes sont identiques,
        </li>
        <li>
les sommes des colonnes sont identiques (à la somme trouvée plus haut),
        </li>
        <li>
les sommes des diagonales sont identiques (à la somme trouvée plus haut).
        </li>
      </ul>
    </p>
  </li>
</ol>

        </div>
      </div>
      <div class="page-header">
      </div>
      <p class="pull-left">
<a href="/sitebp/">retour à la page d'accueil </a><i class="icon-home"></i>
      </p>
      <p class="pull-right">
<i class="icon-arrow-up"></i><a href="#"> retour au sommet</a>
      </p>
    </div>
  </body>
</html>

