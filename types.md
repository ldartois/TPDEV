<!DOCTYPE html>
<html lang="fr">
<h3>
Types
</h3>

<div class="accordion" id="rappels">
  <div class="accordion-group">
    <div class="accordion-heading">
      <a class="accordion-toggle" href="#rappel1"
         data-parent="#rappels" data-toggle="collapse">
        <span class="label label-info">
          <i class="icon-search icon-white">
          </i>
&Agrave; savoir&nbsp;
        </span>
Les types primitifs
      </a>
    </div>
    <div id="rappel1" class="accordion-body collapse">
      <div class="accordion-inner">
        <p>
Voici tous les types entiers prédéfinis en C :
          <table class="table table-striped">
            <thead>
<tr><th>Type</th>                           <th>Nombre usuel de bits</th><th>Formats</th>                   <th>Exemple</th></tr>
            </thead>
            <tbody>
<tr><td><tt>signed char</tt></td>           <td>8</td>                   <td>%c, %hhd, %hhi</td>            <td>'Y'</td>    </tr>
<tr><td><tt>unsigned char</tt></td>         <td>8</td>                   <td>%c, %hhu, %hho, %hhx, %hhX</td><td>'z'</td>    </tr>
<tr><td><tt>short int</tt></td>             <td>16</td>                  <td>%hd, %hi</td>                  <td></td>       </tr>
<tr><td><tt>unsigned short int</tt></td>    <td>16</td>                  <td>%hu, %ho, %hx, %hX</td>        <td></td>       </tr>
<tr><td><tt>int</tt></td>                   <td>32</td>                  <td>%d, %i</td>                    <td>-24</td>    </tr>
<tr><td><tt>unsigned int</tt></td>          <td>32</td>                  <td>%u, %o, %x, %X</td>            <td>97U</td>    </tr>
<tr><td><tt>long int</tt></td>              <td>64</td>                  <td>%ld, %li</td>                  <td>-148L</td>  </tr>
<tr><td><tt>unsigned long int</tt></td>     <td>64</td>                  <td>%lu, %lo, %lx, %lX</td>        <td>4UL</td>    </tr>
<!-- pas de long long int en C89 !!!
<tr><td><tt>long long int</tt></td>         <td>64</td>                  <td>%lld, %lli</td>                <td>-19LL</td>  </tr>
<tr><td><tt>unsigned long long int</tt></td><td>64</td>                  <td>%llu, %llo, %llx, %llX</td>    <td>1452ULL</td></tr>
-->
            </tbody>
          </table>
        </p>
        <p>
Et voici tous les types réels prédéfinis en C :
          <table class="table table-striped">
            <thead>
<tr><th>Type</th>                           <th>Nombre usuel de bits</th><th>Formats</th>                    <th>Exemple</th></tr>
            </thead>
            <tbody>
<tr><td><tt>float</tt></td>                 <td>32</td>                   <td>%f, %e, %E, %g, %a</td>        <td>1.5f</td>   </tr>
<tr><td><tt>double</tt></td>                <td>64</td>                   <td>%lf, %le, %lE, %lg, %la</td>   <td>87.2</td>   </tr>
<tr><td><tt>long double</tt></td>           <td>128</td>                  <td>%Lf, %Le, %LE, %Lg, %La</td>   <td>4.61L</td>  </tr>
            </tbody>
          </table>

        </p>
        <p>
Notez bien que <tt>printf</tt> utilise les formats des <tt>float</tt> pour les
<tt>double</tt> et ne permet pas d'afficher les <tt>float</tt>.
        </p>
      </div>
    </div>
  </div>
  <div class="accordion-group">
    <div class="accordion-heading">
      <a class="accordion-toggle" href="#rappel2"
         data-toggle="collapse" data-parent="#rappels">
        <span class="label label-info">
          <i class="icon-search icon-white">
          </i>
&Agrave; savoir&nbsp;
        </span>
Les conversions
      </a>
    </div>
    <div id="rappel2" class="accordion-body collapse">
      <div class="accordion-inner">
        <p>
Lorsque le type d'une donnée ne correspond pas à l'usage que l'on souhaite en faire, il
est possible de la <em>convertir</em>, c'est à dire de former une nouvelle valeur d'un
autre type aussi proche que possible de la valeur de départ. Le terme technique est
<em>transtypage</em>, mais on utilise aussi <em>conversion</em> ou <em>coercition</em>.
        </p>
        <p>
Il arrive bien souvent qu'une telle conversion se produise sans qu'on l'ait demandée :
elle est alors <em>implicite</em>.
 <pre class="c source-c"><span class="kw4">double</span> x <span class="sy0">=</span> <span class="nu0">1</span><span class="sy0">;</span></pre>Dans cet exemple, la notation choisie pour la valeur <tt>1</tt> sous-entend qu'il s'agit
d'un entier. Mais une variable de type <tt>double</tt> ne peut pas recevoir un entier,
donc avant que l'affectation puisse avoir lieu il faudra convertir la valeur <tt>1</tt>
en une valeur de type <tt>double</tt>.
        </p>
        <p>
Même si le compilateur est capable de détecter ces situations et de produire un programme
opérationnel malgré de telles incohérences dans le fichier source, il n'est pas
souhaitable de s'appuyer sur ce mécanisme.
        </p>
        <p>
En premier lieu, parce que cela peut dissimuler des erreurs : l'exemple précédent était
peut-être dû à un copier-coller malheureux oubliant la partie décimale. Dans ce cas, nous
préférerions que le compilateur nous signale le problème plutôt que de le corriger
incorrectement et silencieusement.
        </p>
        <p>
Et surtout, s'appuyer sur des mécanismes implicites nuit à la lisibilité. Un lecteur peu
attentif risque de ne pas réaliser ce que fait vraiment le programme. Au cas où une
conversion peu judicieuse cause un problème, il sera très difficile de corriger du code
invisible !
        </p>
        <p>
 <pre class="c source-c"><span class="kw4">double</span> x <span class="sy0">=</span> <span class="br0">&#40;</span><span class="kw4">double</span><span class="br0">&#41;</span> <span class="nu0">1</span><span class="sy0">;</span></pre>En supposant que la conversion soit assumée et volontaire, mieux vaut la rendre
<em>explicite</em>. L'opérateur de conversion s'écrit comme un type entre parenthèses. Il
convertit la valeur qui vient immédiatement après dans le type indiqué.
        </p>
        <p>
<span class="label label-warning">Remarque</span> Cet opérateur est plus prioritaire que
les opérations arithmétiques. En cas de doute, n'hésitez pas à user de parenthèses pour
clarifier l'ordre des calculs.
 <pre class="c source-c"><span class="kw4">double</span> x <span class="sy0">=</span> <span class="br0">&#40;</span><span class="kw4">double</span><span class="br0">&#41;</span>  <span class="nu0">18</span><span class="sy0">/</span><span class="nu0">4</span><span class="sy0">;</span>  <span class="coMULTI">/* donne 4.5 */</span>
<span class="kw4">double</span> y <span class="sy0">=</span> <span class="br0">&#40;</span><span class="kw4">double</span><span class="br0">&#41;</span> <span class="br0">&#40;</span><span class="nu0">18</span><span class="sy0">/</span><span class="nu0">4</span><span class="br0">&#41;</span><span class="sy0">;</span> <span class="coMULTI">/* donne 4.0 */</span></pre>        </p>
      </div>
    </div>
  </div>
</div>

<ol>
  <li>
    <p>
<strong>Variété.</strong> Écrivez un programme qui affiche la valeur <tt>77</tt> autant
de fois que possible, en utilisant à chaque fois un type différent.
    </p>
  </li>
  <li>
    <p>
<strong>Débordement.</strong> Écrivez un programme qui demande à l'utilisateur un entier,
le stocke dans une variable de type <tt>int</tt> puis dans une variable de type
<tt>char</tt> avant de l'afficher.
    </p>
    <p>
Testez votre programme en entrant les valeurs <tt>65</tt>, <tt>-65</tt>, <tt>191</tt>,
puis <tt>321</tt>. Comprenez-vous comment la conversion fonctionne ?
    </p>
  </li>
  <li>
    <p>
<strong>Affluence.</strong> Un magasin est ouvert du lundi au vendredi. Chaque jour, un
employé compte le nombre de visiteurs à franchir ses portes, afin d'avoir une idée de la
fréquentation moyenne.
    </p>
    <p>
Écrivez un programme qui demande à l'utilisateur le nombre de visiteurs pour chaque jour
de la semaine, puis affiche le nombre moyen de visiteurs par jour.
    </p>
  </li>
  <li>
    <p>
<i class="icon-time"></i> <strong>Monnaie.</strong> Ce même magasin vend <tt>5,49€</tt>
un produit très demandé, mais il n'accepte ni chèque, ni carte de paiement. Ses caisses
n'ont que des pièces de <tt>2€</tt>, <tt>20¢</tt> et <tt>1¢</tt>.
    </p>
    <p>
Écrivez un programme qui demande à l'utilisateur combien le client a payé, puis affiche
comment rendre la monnaie avec le moins de pièces possible.
    </p>
  </li>
</ol>

        </div>
      </div>
      <div class="page-header">
      </div>
      <p class="pull-left">
<a href="/sitebp/">retour à la page d'accueil </a><i class="icon-home"></i>
      </p>
      <p class="pull-right">
<i class="icon-arrow-up"></i><a href="#"> retour au sommet</a>
      </p>
    </div>
  </body>
</html>

