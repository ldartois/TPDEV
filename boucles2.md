<!DOCTYPE html>
<html lang="fr">


<h3>
Boucles (suite)
</h3>

<div class="accordion" id="rappels">
  <div class="accordion-group">
    <div class="accordion-heading">
      <a class="accordion-toggle" href="#rappel1"
         data-parent="#rappels" data-toggle="collapse">
        <span class="label label-info">
          <i class="icon-search icon-white">
          </i>
&Agrave; savoir&nbsp;
        </span>
Les opérateurs raccourcis
      </a>
    </div>
    <div id="rappel1" class="accordion-body collapse">
      <div class="accordion-inner">
        <p>
Pour faire évoluer la valeur contenue dans une variable, il faut extraire l'ancienne
valeur, faire un calcul pour trouver la nouvelle valeur, puis affecter celle-ci dans la
variable.
<pre class="c source-c">variable <span class="sy0">=</span> variable <span class="sy0">*</span> <span class="nu0">2</span><span class="sy0">;</span></pre>Cette série d'étapes est tellement fréquemment employée que le langage C propose des
opérateurs dédiés dont la notation est raccourcie :
<pre class="c source-c">variable <span class="sy0">*=</span> <span class="nu0">2</span><span class="sy0">;</span></pre>il y a un opérateur raccourci pour chaque opérateur arithmétique, entre.
        </p>
        <p>
<span class="label label-warning">Remarque</span> Tout comme l'affectation normale, ces
opérateurs sont généralement utilisés comme des <em>instructions</em>, c'est à dire
qu'ils ont leur propre ligne, qu'ils sont terminés par un point-virgule, et que leur
résultat est volontairement ignoré (nous ne parlons pas ici de son effet sur la
variable).
        </p>
        <p>
Si vous choisissez d'utiliser un tel opérateur au sein d'une <em>expression</em>, sachez
que son résultat est égal à la valeur affectée.
        </p>
        <p>
Une situation encore plus fréquente est l'<em>incrémentation</em> : lorsque la valeur
contenue dans la variable doit être augmentée de 1.
<pre class="c source-c">variable <span class="sy0">+=</span> <span class="nu0">1</span><span class="sy0">;</span></pre>On dispose d'un opérateur encore plus raccourci dans ce cas :
<pre class="c source-c"><span class="sy0">++</span>variable<span class="sy0">;</span></pre>Là encore, cette opération produit un résultat égal à la valeur affectée, qui est le plus
souvent ignoré (l'important étant surtout l'effet sur la variable). Mais il existe une
variante dont le résultat est <em>l'ancienne valeur</em> de la variable :
<pre class="c source-c">variable<span class="sy0">++;</span></pre>La <em>décrémentation</em> est assurée de la même façon par les deux opérateurs
<tt>--</tt>.
<pre class="c source-c"><span class="kw4">int</span> i <span class="sy0">=</span> <span class="nu0">12</span><span class="sy0">;</span>
<span class="kw3">printf</span><span class="br0">&#40;</span><span class="st0">&quot;%d&quot;</span><span class="sy0">,</span> i<span class="sy0">--</span><span class="br0">&#41;</span><span class="sy0">;</span>
<span class="kw3">printf</span><span class="br0">&#40;</span><span class="st0">&quot;%d&quot;</span><span class="sy0">,</span> <span class="sy0">--</span>i<span class="br0">&#41;</span><span class="sy0">;</span></pre>        </p>
      </div>
    </div>
  </div>
  <div class="accordion-group">
    <div class="accordion-heading">
      <a class="accordion-toggle" href="#rappel2"
         data-toggle="collapse" data-parent="#rappels">
        <span class="label label-info">
          <i class="icon-search icon-white">
          </i>
&Agrave; savoir&nbsp;
        </span>
Le générateur pseudo-aléatoire
      </a>
    </div>
    <div id="rappel2" class="accordion-body collapse">
      <div class="accordion-inner">
        <p>
Dans la bibliothèque standard (et l'en-tête <code>stdlib.h</code>), on trouve une
fonction qui permet de choisir un nombre au hasard, avec quelques réserves.
<pre class="c source-c"><span class="kw1">for</span><span class="br0">&#40;</span>i <span class="sy0">=</span> <span class="nu0">0</span><span class="sy0">;</span> i <span class="sy0">&lt;</span> <span class="nu0">10</span><span class="sy0">;</span> i<span class="sy0">++</span><span class="br0">&#41;</span> <span class="br0">&#123;</span>
  <span class="kw3">printf</span><span class="br0">&#40;</span><span class="st0">&quot;%d<span class="es1">\n</span>&quot;</span><span class="sy0">,</span> <span class="kw3">rand</span><span class="br0">&#40;</span><span class="br0">&#41;</span><span class="br0">&#41;</span><span class="sy0">;</span>
<span class="br0">&#125;</span></pre>La fonction <tt>rand</tt> (la contraction de random, qui signifie hasard en anglais)
produit une valeur différente à chaque appel, choisie (en apparence) aléatoirement entre
<tt>0</tt> et une limite supérieure imposée (représentée par la constante
<tt>RAND_MAX</tt>).
        </p>
        <p>
Si vous exécutez plusieurs fois l'exemple précédent, vous constaterez que la même liste
de valeurs est produite à chaque fois. C'est parce que rien n'est réellement aléatoire
dans le fonctionnement normal d'un ordinateur. Pour simuler un choix au hasard, la
fonction <tt>rand</tt> pioche en fait dans une série prédéterminée.
        </p>
        <p>
Pour éviter d'être trop prévisible, on peut demander à <tt>rand</tt> de faire partir la
série d'un endroit différent. C'est le but de la fonction <tt>srand</tt>, qui fixe la
<em>graine</em> (le point de départ) du générateur pseudo-aléatoire.
<pre class="c source-c"><span class="kw3">srand</span><span class="br0">&#40;</span><span class="nu0">1478245</span><span class="br0">&#41;</span><span class="sy0">;</span>
<span class="kw1">for</span><span class="br0">&#40;</span>i <span class="sy0">=</span> <span class="nu0">0</span><span class="sy0">;</span> i <span class="sy0">&lt;</span> <span class="nu0">10</span><span class="sy0">;</span> i<span class="sy0">++</span><span class="br0">&#41;</span> <span class="br0">&#123;</span>
  <span class="kw3">printf</span><span class="br0">&#40;</span><span class="st0">&quot;%d<span class="es1">\n</span>&quot;</span><span class="sy0">,</span> <span class="kw3">rand</span><span class="br0">&#40;</span><span class="br0">&#41;</span><span class="br0">&#41;</span><span class="sy0">;</span>
<span class="br0">&#125;</span></pre>Ce nouvel exemple produit une liste de valeurs différente, mais plusieurs exécutions
obtiennent tout de même un affichage identique. Une technique souvent employée pour faire
varier la graine d'une exécution à l'autre consiste à la choisir en fonction de l'heure.
        </p>
        <p>
<pre class="c source-c"><span class="kw3">srand</span><span class="br0">&#40;</span><span class="kw3">time</span><span class="br0">&#40;</span>NULL<span class="br0">&#41;</span><span class="br0">&#41;</span><span class="sy0">;</span>
<span class="kw1">for</span><span class="br0">&#40;</span>i <span class="sy0">=</span> <span class="nu0">0</span><span class="sy0">;</span> i <span class="sy0">&lt;</span> <span class="nu0">10</span><span class="sy0">;</span> i<span class="sy0">++</span><span class="br0">&#41;</span> <span class="br0">&#123;</span>
  <span class="kw3">printf</span><span class="br0">&#40;</span><span class="st0">&quot;%d<span class="es1">\n</span>&quot;</span><span class="sy0">,</span> <span class="kw3">rand</span><span class="br0">&#40;</span><span class="br0">&#41;</span><span class="br0">&#41;</span><span class="sy0">;</span>
<span class="br0">&#125;</span></pre>La fonction <tt>time</tt>, qui est définie dans la bibliothèque standard et dans
l'en-tête <code>time.h</code>, renvoie l'heure courante en secondes. Tant qu'il s'écoule
au moins une seconde entre deux exécutions, chaque lancement du programme produira donc
des valeurs entièrement différentes.
        </p>
        <p>
<span class="label label-warning">Remarque</span> Les valeurs &laquo;aléatoires&raquo;
ainsi obtenues sont imprévisibles pour un utilisateur moyen, mais un investigateur
appliqué peut quand même parvenir à anticiper les résultats, donc cette méthode est
insuffisante pour les jeux d'argent ou les systèmes de sécurité !
        </p>
      </div>
    </div>
  </div>
</div>

<ol>
  <li>
    <p>
<strong>Devinette.</strong> Écrivez un programme qui donne cinq tentatives à
l'utilisateur pour deviner un nombre entre 0 et 100. À chaque tentative infructueuse, le
programme offrira un indice en affichant <tt>+</tt> ou <tt>-</tt>.
    </p>
    <p>
Utilisez des constantes nommées pour représenter le nombre de tentatives et la valeur
maximum.
    </p>
  </li>
  <li>
    <p>
<strong>Primarité.</strong> Écrivez un programme qui demande à l'utilisateur un entier
naturel puis indique si cet entier est premier.
    </p>
    <p>
<span class="label label-warning">Remarque</span> Un entier est premier si et seulement
si il admet exactement deux diviseurs stricts : 1 et lui-même.
    </p>
  </li>
  <li>
    <p>
<strong>Table.</strong> Écrivez un programme qui affiche la table de multiplication.
<pre>
  X  |   0   1   2   3   4   5   6   7   8   9  10
-----+--------------------------------------------
  0  |   0   0   0   0   0   0   0   0   0   0   0
  1  |   0   1   2   3   4   5   6   7   8   9  10
  2  |   0   2   4   6   8  10  12  14  16  18  20
  3  |   0   3   6   9  12  15  18  21  24  27  30
  4  |   0   4   8  12  16  20  24  28  32  36  40
  5  |   0   5  10  15  20  25  30  35  40  45  50
  6  |   0   6  12  18  24  30  36  42  48  54  60
  7  |   0   7  14  21  28  35  42  49  56  63  70
  8  |   0   8  16  24  32  40  48  56  64  72  80
  9  |   0   9  18  27  36  45  54  63  72  81  90
 10  |   0  10  20  30  40  50  60  70  80  90 100
</pre>
Utilisez dans votre programme une constante nommée pour représenter la taille de la table
(10 dans l'exemple ci-dessus). Votre programme devra encore fonctionner si on choisit une
taille différente en changeant la valeur de cette constante.
    </p>
  </li>
  <li>
    <p>
<strong>Progression.</strong> Ecrire un programme qui affiche le <tt>n</tt>ième terme de
la suite de Fibonacci, définie par la relation de récurrence suivante :
    </p>
    <p style="text-align:center">
<img width="300" src="fibonacci.svg" alt=""/>
    </p>
  </li>
  <li>
    <p>
<i class="icon-time"></i> <strong>Figures.</strong> Un théorème dû à Lagrange affirme que
tout entier naturel peut s'écrire comme la somme de quatre carrés. Par exemple :
<pre>
28=25+1+1+1
28=16+4+4+4
28=9+9+9+1
</pre>
Ou encore :
<pre>
10=9+1+0+0
10=4+4+1+1
</pre>
Écrivez un programme qui demande un entier naturel, et affiche toutes ses décompositions
sous la forme d'une somme de 4 carrés (attention, chaque décomposition ne doit apparaître
qu'une seule fois).
    </p>
  </li>
  <li>
    <p>
<i class="icon-time"></i> <strong>Facteurs.</strong> Écrivez un programme qui demande un
entier naturel non nul, et affiche sa décomposition en facteurs premiers. Par exemple, 
<pre>
Entrez un entier naturel non nul : 280
280 = 2*2*2*5*7
</pre>
<span class="label label-warning">Remarque</span> Pour tous les entiers naturels, le plus
petit diviseur autre que 1 est nécessairement premier.
    </p>
  </li>
</ol>
<a href="/sitebp/">retour à la page d'accueil </a><i class="icon-home"></i>
      </p>
      <p class="pull-right">
<i class="icon-arrow-up"></i><a href="#"> retour au sommet</a>
      </p>
    </div>
  </body>
</html>

