<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>
DEV 1.1
    </title>
    </style>
  </head>
  <body>
<h3>
Tableaux
</h3>

<div class="accordion" id="rappels">
  <div class="accordion-group">
    <div class="accordion-heading">
      <a class="accordion-toggle" href="#rappel1"
         data-parent="#rappels" data-toggle="collapse">
        <span class="label label-info">
          <i class="icon-search icon-white">
          </i>
&Agrave; savoir&nbsp;
        </span>
La déclaration de tableau
      </a>
    </div>
    <div id="rappel1" class="accordion-body collapse">
      <div class="accordion-inner">
        <p>
Un tableau est un moyen de réserver beaucoup de mémoire pour pouvoir stocker un
grand volume de données.
<pre class="c source-c"><span class="kw4">int</span> main<span class="br0">&#40;</span><span class="kw4">void</span><span class="br0">&#41;</span> <span class="br0">&#123;</span>
  <span class="kw4">int</span> tab<span class="br0">&#91;</span><span class="nu0">50</span><span class="br0">&#93;</span> <span class="sy0">=</span> <span class="br0">&#123;</span><span class="nu0">12</span><span class="sy0">,</span> <span class="nu0">34</span><span class="sy0">,</span> <span class="nu0">56</span><span class="sy0">,</span> <span class="nu0">78</span><span class="br0">&#125;</span><span class="sy0">;</span>
  ...
<span class="br0">&#125;</span></pre>Dans cet exemple, la déclaration a l'effet suivant :
          <ul>
            <li>
un tableau nommé <tt>tab</tt> est déclaré (c'est une sorte de variable),
            </li>
            <li>
une zone continue de mémoire, suffisante pour contenir 50 valeurs de type <tt>int</tt>,
est réservée,
            </li>
            <li>
les quatre premières cases du tableau recoivent les valeurs fournies dans la liste
d'initialisation,
            </li>
            <li>
les cases suivantes recoivent la valeur <tt>0</tt>.
            </li>
          </ul>
        </p>
        <p>
Les deux dernières étapes n'ont pas lieu si vous choisissez de ne pas initialiser le
tableau. La dernière étape n'a pas lieu si le nombre de valeurs fournies suffit à
initialiser toutes les cases (mais le compilateur proteste si la liste contient trop de
valeurs).
        </p>
        <p>
La valeur placée entre crochets est la <em>capacité</em> du tableau, c'est à dire le
nombre de cases qu'il contient. Dans la norme C89, seule une capacité constante est
permise (<code>gcc</code> a une autre opinion sur la question, mais nous choisissons de
respecter le standard). Si vous omettez la capacité, elle est égale à la longueur de la
liste d'initialisation.
        </p>
        <p>
Chaque case du tableau se comporte comme une variable, mais ne possède pas
d'identificateur. On peut y stocker une valeur, mais pour y accéder il faut passer par
une notation spéciale (voir plus loin).
        </p>
        <p>
La liste d'initialisation, qui est optionnelle, est formée impérativement de constantes,
séparées par des virgules, et encadrées par des accolades. Une telle liste ne peut être
utilisée qu'au moment de la déclaration d'un tableau, et donc jamais dans une
affectation.
        </p>
      </div>
    </div>
  </div>
  <div class="accordion-group">
    <div class="accordion-heading">
      <a class="accordion-toggle" href="#rappel2"
         data-parent="#rappels" data-toggle="collapse">
        <span class="label label-info">
          <i class="icon-search icon-white">
          </i>
&Agrave; savoir&nbsp;
        </span>
L'opérateur <tt>[]</tt>
      </a>
    </div>
    <div id="rappel2" class="accordion-body collapse">
      <div class="accordion-inner">
        <p>
Pour manipuler les cases d'un tableau, il faut en connaître l'adresse. L'opérateur
<tt>[]</tt> permet de calculer puis d'exploiter l'adresse d'une case.
<pre class="c source-c"><span class="kw4">int</span> main<span class="br0">&#40;</span><span class="kw4">void</span><span class="br0">&#41;</span> <span class="br0">&#123;</span>
  <span class="kw4">char</span> message<span class="br0">&#91;</span><span class="br0">&#93;</span> <span class="sy0">=</span> <span class="br0">&#123;</span><span class="st0">'h'</span><span class="sy0">,</span> <span class="st0">'e'</span><span class="sy0">,</span> <span class="st0">'l'</span><span class="sy0">,</span> <span class="st0">'l'</span><span class="sy0">,</span> <span class="st0">'o'</span><span class="br0">&#125;</span><span class="sy0">;</span>
  <span class="kw4">int</span> i<span class="sy0">;</span>
&nbsp;
  <span class="kw1">for</span><span class="br0">&#40;</span>i <span class="sy0">=</span> <span class="nu0">0</span><span class="sy0">;</span> i <span class="sy0">&lt;</span> <span class="nu0">5</span><span class="sy0">;</span> i<span class="sy0">++</span><span class="br0">&#41;</span>
    <span class="kw3">putchar</span><span class="br0">&#40;</span>message<span class="br0">&#91;</span>i<span class="br0">&#93;</span><span class="br0">&#41;</span><span class="sy0">;</span>
  <span class="kw3">putchar</span><span class="br0">&#40;</span><span class="st0">'<span class="es1">\n</span>'</span><span class="br0">&#41;</span><span class="sy0">;</span>
  <span class="kw1">return</span> EXIT_SUCCESS<span class="sy0">;</span>
<span class="br0">&#125;</span></pre>Cet opérateur a besoin (à droite) du tableau qui contient la donnée et (entre les
crochets) de l'indice de la case. Cet indice doit toujours être supérieur ou égal à 0 et
<strong>strictement inférieur à la capacité</strong>. On obtient comme résultat la case
demandée, qui peut ensuite être utilisée comme une variable (en lecture comme en
écriture).
        </p>
        <p>
<span class="label label-warning">Remarque</span> L'opérateur <tt>[]</tt> permet de faire
des erreurs très dangereuses. Très régulièrement les programmeurs débutants lui
fournissent de mauvais indices. Ce problème, appelé <em>dépassement de capacité</em>, est
une source fréquente de bogues coriaces.
        </p>
      </div>
    </div>
  </div>
</div>

<ol>
  <li>
    <p>
<strong>Remplissage.</strong> Écrivez un programme qui déclare un tableau de 10 cases
de type <tt>int</tt>, puis le remplit avec des valeurs choisies aléatoirement entre -50
et 50, et enfin affiche le contenu du tableau.
    </p>
<pre style="line-height:110%">
bob@box:~$ ./a.out
+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
|  12 |   0 | -49 |  35 |   8 |  -2 |  50 |  33 | -40 |  -1 |
+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
</pre>
    <p>
<span class="label label-warning">Remarque</span> Les fonctions nécessaires pour la
génération pseudo-aléatoire sont décrites dans <a href="/sitebp/dev11/boucles2/">le
deuxième sujet sur les boucles</a>.
    </p>
  </li>
  <li>
    <p>
<strong>Maximum.</strong> Modifiez le programme de l'exercice précédent pour qu'il
identifie le plus grand élément du tableau.
    </p>
<pre style="line-height:110%">
bob@box:~$ ./a.out
                           |
                           V
+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
|  27 | -33 | -12 | -37 |  48 |  -7 |   0 | -50 |  -3 | -13 |
+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
</pre>
  </li>
  <li>
    <p>
<strong>Recherche.</strong> Modifiez le programme du premier exercice pour qu'après
l'affichage du tableau, l'utilisateur puisse entrer une valeur. Le programme affichera
alors le plus petit indice où on peut trouver cette valeur dans le tableau (ou
<tt>-1</tt> si la valeur ne s'y trouve pas).
    </p>
  </li>
  <li>
    <p>
<strong>Miroir.</strong> Modifiez le programme du premier exercice pour qu'il inverse
l'ordre des valeurs du tableau. Vous afficherez le tableau avant et après inversion pour
vérifier votre travail.
    </p>
  </li>
  <li>
    <p>
<i class="icon-time"></i> <strong>Filtre.</strong> Reprenez le programme du premier
exercice. Après avoir affiché le tableau, retirez-en toutes les valeurs négatives (sans
changer l'ordre des autres valeurs) puis affichez-le à nouveau.
    </p>
  </li>
  <li>
    <p>
<i class="icon-time"></i> <strong>Circulation.</strong> Modifiez le programme du premier
exercice pour qu'il avance les valeurs du tableau de 4 indices (les valeurs à la fin se
retrouveront au début). Vous afficherez le tableau avant et après permutation pour
vérifier votre travail.
    </p>
    <p>
<span class="label label-warning">Remarque</span> La valeur 4 est arbitraire. Faites-en
une constante nommée et assurez-vous que le programme marche aussi pour d'autres valeurs
de décalage.
    </p>
  </li>
<!--
  <li>
    <p>
<strong>Statistiques.</strong> On souhaite analyser les résultats d'une épreuve à
laquelle ont participé 25 étudiants. Remplissez aléatoirement un tableau avec 25 notes
comprises entre 0 et 20 (précises au quart de point). Affichez ensuite :
      <ul>
        <li>
la plus haute note,
        </li>
        <li>
la plus basse note,
        </li>
        <li>
la moyenne,
        </li>
        <li>
l'écart type.
        </li>
      </ul>
    </p>
  </li>
  <li>
    <p>
<strong>Histogramme.</strong> Reprenez le programme de l'exercice précédent et ajoutez-y
un tableau dont les cases (de type <tt>unsigned short</tt>) contiennent :
      <ul>
        <li>
le nombre de notes &isin; [0;4[
        </li>
        <li>
le nombre de notes &isin; [4;8[
        </li>
        <li>
le nombre de notes &isin; [8;12[
        </li>
        <li>
le nombre de notes &isin; [12;16[
        </li>
        <li>
le nombre de notes &isin; [16;20]
        </li>
      </ul>
    </p>
    <p>
Affichez ensuite ce tableau sous la forme d'un graphique, comme ceci :
<pre>
7 >                 #######
6 >                 ####### 
5 >         ####### ####### #######
4 > ####### ####### ####### ####### #######
3 > ####### ####### ####### ####### #######
2 > ####### ####### ####### ####### #######
1 > ####### ####### ####### ####### #######
   +-------+-------+-------+-------+-------+
     0 - 4   4 - 8   8 -12   12-16   16-20
</pre>
    </p>
  </li>
  <li>
    <p>
<strong>Tri.</strong> Reprenez le programme du premier exercice. Après avoir affiché le
tableau, rangez ses valeurs par ordre croissant puis affichez-le à nouveau.
    </p>
    <p>
La méthode de tri à employer est le <em>tri par sélection</em> :
      <ul>
        <li>
on cherche la plus grande valeur.
        </li>
        <li>
on l'échange avec la dernière valeur.
        </li>
        <li>
on recommence (en ignorant les valeurs déjà placées) jusqu'à avoir tout placé.
        </li>
      </ul>
    </p>
    <p class="text-center">
      <div class="row">
        <div class="span7 offset1">
          <table class="table table-bordered">
            <colgroup span="10" style="width:10%">
            </colgroup>
            <tbody>
              <tr>
                <td>
<div class="text-center">-28</div>
                </td>
                <td>
<div class="text-center">-1</div>
                </td>
                <td>
<div class="text-center">15</div>
                </td>
                <td>
<div class="text-center">7</div>
                </td>
                <td>
<div class="text-center text-error">48</div>
                </td>
                <td>
<div class="text-center">-14</div>
                </td>
                <td>
<div class="text-center">31</div>
                </td>
                <td>
<div class="text-center">1</div>
                </td>
                <td>
<div class="text-center">-14</div>
                </td>
                <td>
<div class="text-center text-info">27</div>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
      <div class="row">
        <div class="span7 offset1">
          <table class="table table-bordered">
            <colgroup span="10" style="width:10%">
            </colgroup>
            <tbody>
              <tr>
                <td>
<div class="text-center">-28</div>
                </td>
                <td>
<div class="text-center">-1</div>
                </td>
                <td>
<div class="text-center">15</div>
                </td>
                <td>
<div class="text-center">7</div>
                </td>
                <td>
<div class="text-center">27</div>
                </td>
                <td>
<div class="text-center">-14</div>
                </td>
                <td>
<div class="text-center text-error">31</div>
                </td>
                <td>
<div class="text-center">1</div>
                </td>
                <td>
<div class="text-center text-info">-14</div>
                </td>
                <td>
<div class="text-center text-success">48</div>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
      <div class="row">
        <div class="span7 offset1">
          <table class="table table-bordered">
            <colgroup span="10" style="width:10%">
            </colgroup>
            <tbody>
              <tr>
                <td>
<div class="text-center">-28</div>
                </td>
                <td>
<div class="text-center">-1</div>
                </td>
                <td>
<div class="text-center">15</div>
                </td>
                <td>
<div class="text-center">7</div>
                </td>
                <td>
<div class="text-center text-error">27</div>
                </td>
                <td>
<div class="text-center">-14</div>
                </td>
                <td>
<div class="text-center">-14</div>
                </td>
                <td>
<div class="text-center text-info">1</div>
                </td>
                <td>
<div class="text-center text-success">31</div>
                </td>
                <td>
<div class="text-center text-success">48</div>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
      <div class="row">
        <div class="span7 offset1">
          <table class="table table-bordered">
            <colgroup span="10" style="width:10%">
            </colgroup>
            <tbody>
              <tr>
                <td>
<div class="text-center">-28</div>
                </td>
                <td>
<div class="text-center">-1</div>
                </td>
                <td>
<div class="text-center text-error">15</div>
                </td>
                <td>
<div class="text-center">7</div>
                </td>
                <td>
<div class="text-center">1</div>
                </td>
                <td>
<div class="text-center">-14</div>
                </td>
                <td>
<div class="text-center text-info">-14</div>
                </td>
                <td>
<div class="text-center text-success">27</div>
                </td>
                <td>
<div class="text-center text-success">31</div>
                </td>
                <td>
<div class="text-center text-success">48</div>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
      <div class="row">
        <div class="span7 offset1">
          <table class="table table-bordered">
            <colgroup span="10" style="width:10%">
            </colgroup>
            <tbody>
              <tr>
                <td>
<div class="text-center">-28</div>
                </td>
                <td>
<div class="text-center">-1</div>
                </td>
                <td>
<div class="text-center">-14</div>
                </td>
                <td>
<div class="text-center text-error">7</div>
                </td>
                <td>
<div class="text-center">1</div>
                </td>
                <td>
<div class="text-center text-info">-14</div>
                </td>
                <td>
<div class="text-center text-success">15</div>
                </td>
                <td>
<div class="text-center text-success">27</div>
                </td>
                <td>
<div class="text-center text-success">31</div>
                </td>
                <td>
<div class="text-center text-success">48</div>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
      <div class="row">
        <div class="span7 offset1">
          <table class="table table-bordered">
            <colgroup span="10" style="width:10%">
            </colgroup>
            <tbody>
              <tr>
                <td>
<div class="text-center">-28</div>
                </td>
                <td>
<div class="text-center">-1</div>
                </td>
                <td>
<div class="text-center">-14</div>
                </td>
                <td>
<div class="text-center">-14</div>
                </td>
                <td>
<div class="text-center text-info">1</div>
                </td>
                <td>
<div class="text-center text-success">7</div>
                </td>
                <td>
<div class="text-center text-success">15</div>
                </td>
                <td>
<div class="text-center text-success">27</div>
                </td>
                <td>
<div class="text-center text-success">31</div>
                </td>
                <td>
<div class="text-center text-success">48</div>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
      <div class="row">
        <div class="span7 offset1">
          <table class="table table-bordered">
            <colgroup span="10" style="width:10%">
            </colgroup>
            <tbody>
              <tr>
                <td>
<div class="text-center">-28</div>
                </td>
                <td>
<div class="text-center text-error">-1</div>
                </td>
                <td>
<div class="text-center">-14</div>
                </td>
                <td>
<div class="text-center text-info">-14</div>
                </td>
                <td>
<div class="text-center text-success">1</div>
                </td>
                <td>
<div class="text-center text-success">7</div>
                </td>
                <td>
<div class="text-center text-success">15</div>
                </td>
                <td>
<div class="text-center text-success">27</div>
                </td>
                <td>
<div class="text-center text-success">31</div>
                </td>
                <td>
<div class="text-center text-success">48</div>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
      <div class="row">
        <div class="span7 offset1">
          <table class="table table-bordered">
            <colgroup span="10" style="width:10%">
            </colgroup>
            <tbody>
              <tr>
                <td>
<div class="text-center">-28</div>
                </td>
                <td>
<div class="text-center text-error">-14</div>
                </td>
                <td>
<div class="text-center text-info">-14</div>
                </td>
                <td>
<div class="text-center text-success">-1</div>
                </td>
                <td>
<div class="text-center text-success">1</div>
                </td>
                <td>
<div class="text-center text-success">7</div>
                </td>
                <td>
<div class="text-center text-success">15</div>
                </td>
                <td>
<div class="text-center text-success">27</div>
                </td>
                <td>
<div class="text-center text-success">31</div>
                </td>
                <td>
<div class="text-center text-success">48</div>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
      <div class="row">
        <div class="span7 offset1">
          <table class="table table-bordered">
            <colgroup span="10" style="width:10%">
            </colgroup>
            <tbody>
              <tr>
                <td>
<div class="text-center">-28</div>
                </td>
                <td>
<div class="text-center text-info">-14</div>
                </td>
                <td>
<div class="text-center text-success">-14</div>
                </td>
                <td>
<div class="text-center text-success">-1</div>
                </td>
                <td>
<div class="text-center text-success">1</div>
                </td>
                <td>
<div class="text-center text-success">7</div>
                </td>
                <td>
<div class="text-center text-success">15</div>
                </td>
                <td>
<div class="text-center text-success">27</div>
                </td>
                <td>
<div class="text-center text-success">31</div>
                </td>
                <td>
<div class="text-center text-success">48</div>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
      <div class="row">
        <div class="span7 offset1">
          <table class="table table-bordered">
            <colgroup span="10" style="width:10%">
            </colgroup>
            <tbody>
              <tr>
                <td>
<div class="text-center text-success">-28</div>
                </td>
                <td>
<div class="text-center text-success">-14</div>
                </td>
                <td>
<div class="text-center text-success">-14</div>
                </td>
                <td>
<div class="text-center text-success">-1</div>
                </td>
                <td>
<div class="text-center text-success">1</div>
                </td>
                <td>
<div class="text-center text-success">7</div>
                </td>
                <td>
<div class="text-center text-success">15</div>
                </td>
                <td>
<div class="text-center text-success">27</div>
                </td>
                <td>
<div class="text-center text-success">31</div>
                </td>
                <td>
<div class="text-center text-success">48</div>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </p>
  </li>
-->
</ol>

        </div>
      </div>
      <div class="page-header">
      </div>
      <p class="pull-left">
<a href="/sitebp/">retour à la page d'accueil </a><i class="icon-home"></i>
      </p>
      <p class="pull-right">
<i class="icon-arrow-up"></i><a href="#"> retour au sommet</a>
      </p>
    </div>
  </body>
</html>

