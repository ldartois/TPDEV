<!DOCTYPE html>

<h3>
Boucles (encore)
</h3>

<ol>
  <li>
    <p>
<strong>Sapin.</strong> Écrivez le programme correspondant à la session suivante :
<pre>
Hauteur ? 6
```
     *
    ***
   ***** 
  *******
 *********
***********
```
</pre>
    </p>
  </li>
  <li>
    <p>
<strong>Figures.</strong> Écrivez le programme correspondant à
la session suivante :
<pre>
_______________
 t) Triangle
 c) Carré
 q) Quitter
Votre choix ? t

Hauteur ? 4
```
*
**
***
****
```
_______________
 t) Triangle
 c) Carré
 q) Quitter
Votre choix ? c

Hauteur ? 5
```
*****
*   *
*   *
*   *
*****
```
_______________
 t) Triangle
 c) Carré
 q) Quitter
Votre choix ? q

Au revoir...
</pre>
    </p>
  </li>
  <li>
    <p>
<strong>Diviseur.</strong> Écrivez un programme qui demande deux entiers naturels à
l'utilisateur, puis affiche leur plus grand commun diviseur.
    </p>
    <p>
L'<em>algorithme d'Euclide</em> nous donne un bon moyen de faire ce calcul :
      <div class="row">
        <div class="span1 offset1" style="font-family:serif">
Si b = 0, <br/>
Sinon, 
        </div>
        <div class="span3" style="font-family:serif">
pgcd(a, b) = a<br/>
pgcd(a, b) = pgcd(b, a mod b)
        </div>
      </div>
    </p>
  </li>
  <li>
    <p>
<i class="icon-time"></i> <strong>Comptes.</strong> On souhaite s'inspirer de la commande
Unix <code>wc</code>.
      <ul>
        <li>
Écrivez un programme qui affiche le nombre de caractères obtenus sur l'entrée standard.
        </li>
        <li>
Modifiez ce programme pour qu'il affiche également le nombre de sauts de ligne obtenus
sur l'entrée standard.
        </li>
        <li>
Ajoutez enfin l'affichage du nombre de mots obtenus sur l'entrée standard.
        </li>
      </ul>
    </p>
    <p>
<span class="label label-warning">Remarque</span> Ce programme doit lire sur l'entrée
standard jusqu'à en atteindre la fin. Si vous avez redirigé l'entrée standard du
programme vers un fichier, la fin de l'entrée standard correspond tout simplement à la
fin du fichier. Si l'entrée standard n'est pas redirigée, vous pouvez indiquer la fin en
tapant <strong>Contrôle D</strong>.
    </p>
    <p>
La fonction <tt>scanf</tt> renvoit <tt>-1</tt> lorsqu'elle atteint la fin de l'entrée
standard.
    </p>
  </li>
  <li>
    <p>
<i class="icon-time"></i> <strong>Disque.</strong> Écrivez un programme qui demande le
rayon, puis affiche un disque formé d'étoiles.
<pre>
Rayon ? 10
             ```                             
            * * * * * * * * *             
          * * * * * * * * * * *           
      * * * * * * * * * * * * * * *       
      * * * * * * * * * * * * * * *       
    * * * * * * * * * * * * * * * * *     
  * * * * * * * * * * * * * * * * * * *   
  * * * * * * * * * * * * * * * * * * *   
  * * * * * * * * * * * * * * * * * * *   
  * * * * * * * * * * * * * * * * * * *   
  * * * * * * * * * * * * * * * * * * *   
  * * * * * * * * * * * * * * * * * * *   
  * * * * * * * * * * * * * * * * * * *   
  * * * * * * * * * * * * * * * * * * *   
  * * * * * * * * * * * * * * * * * * *   
    * * * * * * * * * * * * * * * * *     
      * * * * * * * * * * * * * * *       
      * * * * * * * * * * * * * * *       
          * * * * * * * * * * *           
            * * * * * * * * *             

          ```                                
</pre>
On se souviendra que les points d'un disque sont les points dont la distance au centre
est inférieure au rayon.
    </p>
  </li>
</ol>

        </div>
      </div>
      <div class="page-header">
      </div>
      <p class="pull-left">
<a href="/sitebp/">retour à la page d'accueil </a><i class="icon-home"></i>
      </p>
      <p class="pull-right">
<i class="icon-arrow-up"></i><a href="#"> retour au sommet</a>
      </p>
    </div>
  </body>
</html>

